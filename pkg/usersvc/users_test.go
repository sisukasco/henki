package usersvc_test

import (
	"context"
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/sisukasco/commons/utils"
	"github.com/sisukasco/henki/pkg/db"
	"github.com/sisukasco/henki/pkg/usersvc"
	"github.com/stretchr/testify/assert"
	"syreclabs.com/go/faker"
)

func TestGettingUsersSignedUpNDaysAgoOneRec(t *testing.T) {
	userSvcObj := usersvc.New(svc)

	twoDaysBack := time.Now().AddDate(0, 0, -2)
	userRec := db.InsertUserCustomParams{
		ID:        faker.RandomString(8),
		Email:     faker.Internet().Email(),
		FirstName: faker.Name().FirstName(),
		LastName:  faker.Name().LastName(),
		CreatedAt: twoDaysBack,
		UpdatedAt: twoDaysBack,
	}

	ctx := context.Background()

	err := svc.DB.Q.ClearUserTable(ctx)
	assert.Nil(t, err)

	t.Logf("userRec %v", utils.ToJSONString(userRec))

	_, err = svc.DB.Q.InsertUserCustom(ctx, userRec)
	assert.Nil(t, err)

	users, err := userSvcObj.GetUsersCreatedNDaysAgo(ctx, 2)
	assert.Nil(t, err)

	t.Logf("Users %v", utils.ToJSONString(users))

	assert.Equal(t, len(users), 1)

	assert.Equal(t, users[0].Email, userRec.Email)

}

func insertUser(daysBack int32) (*db.InsertUserCustomParams, error) {
	ctx := context.Background()

	daysBackObj := time.Now().AddDate(0, 0, int(-1*daysBack)).Add(-1 * time.Duration(rand.Intn(12)) * time.Minute)

	userRec := db.InsertUserCustomParams{
		ID:        faker.RandomString(8),
		Email:     faker.Internet().Email(),
		FirstName: faker.Name().FirstName(),
		LastName:  faker.Name().LastName(),
		CreatedAt: daysBackObj,
		UpdatedAt: daysBackObj,
	}
	_, err := svc.DB.Q.InsertUserCustom(ctx, userRec)
	if err != nil {
		return nil, err
	}

	fmt.Printf("user %d days ago \n %v", daysBack, utils.ToJSONString(userRec))

	return &userRec, nil
}

func TestGettingUsersSignedUpNDaysAgoMultipleRec(t *testing.T) {
	userSvcObj := usersvc.New(svc)

	ctx := context.Background()
	err := svc.DB.Q.ClearUserTable(ctx)
	assert.Nil(t, err)

	userRec, err := insertUser(2)
	assert.Nil(t, err)

	_, err = insertUser(3)
	assert.Nil(t, err)

	_, err = insertUser(1)
	assert.Nil(t, err)

	{
		users, err := userSvcObj.GetUsersCreatedNDaysAgo(ctx, 2)
		assert.Nil(t, err)

		t.Logf("Users  2 days ago \n %v", utils.ToJSONString(users))

		assert.Equal(t, len(users), 1)

		assert.Equal(t, users[0].Email, userRec.Email)
	}

	{
		users, err := userSvcObj.GetUsersCreatedNDaysAgo(ctx, 3)
		assert.Nil(t, err)

		t.Logf("Users  3 days ago \n %v", utils.ToJSONString(users))

		assert.Equal(t, len(users), 1)

	}

	{
		users, err := userSvcObj.GetUsersCreatedNDaysAgo(ctx, 1)
		assert.Nil(t, err)

		t.Logf("Users  1 days ago \n %v", utils.ToJSONString(users))

		assert.Equal(t, len(users), 1)

	}

}

func TestGettingUsersSignedUpNDaysMultipleUsers(t *testing.T) {
	userSvcObj := usersvc.New(svc)

	ctx := context.Background()
	err := svc.DB.Q.ClearUserTable(ctx)
	assert.Nil(t, err)

	for u := 0; u < 10; u++ {
		_, err := insertUser(2)
		assert.Nil(t, err)
	}

	users, err := userSvcObj.GetUsersCreatedNDaysAgo(ctx, 2)
	assert.Nil(t, err)

	t.Logf("Users  2 days ago \n %v", utils.ToJSONString(users))

	assert.Equal(t, len(users), 10)

}
